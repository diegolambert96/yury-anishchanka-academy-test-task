function calculateRoverPath(map)
{
    let m = arr.length-1,
    fs = require('fs'),
    n = 0,        
    start = '0,0',
    graph = {},
    goal = 0;    
   
    for(let i = 0; i < arr.length; i++) {//check size 
        if(arr[i].length > n) 
            n = arr[i].length-1;
    }
    goal = (m)+','+(n); 
    
    arr.forEach((el,y) =>   {   //y => i
        el.forEach((el2,x) =>   { //x => j
            let index;
            index = [y,x].join(",");
            graph[index] = [];                     
            for(let i = -1; i<=1; i++)  {
                for( let j = -1; j<=1; j++) {
                    if( (j!= 0 || i!= 0) && arr[y+i])
                    {                   
                        if(Math.abs(i) != Math.abs(j))  { //non-diagonal value
                            cost = Math.max(arr[y+i][x+j],el2)-Math.min(arr[y+i][x+j],el2)+(+1);
                            if(cost)    {
                                    graph[index].push([cost,[y+i,x+j].join(',')] );
                            }    
                        }
                    }
                }
            }             
        })
    })
    
    let visited = findway(start,goal,graph);
    
    if(visited)    {
        let steps = 0;
        let current = goal;
        let points = [goal];
        while(current != start)        {
            current = visited[current]
            points.push(current);
            steps += 1;
        }
        let str = ('['+points.reverse().join(']->[')+']').replace(/,/g,'][');
        str += '\nsteps: '+steps+'\nfuel: '+fuel;
        fs.writeFileSync('path-plan.txt',str);
    }   else    {
        fs.writeFileSync('path-plan.txt','way not found!');
        }
    return findway(start,goal,graph)
}    function findway(start,goal,graph) {
    let processed = [];
    let prices = {[start] : 0};
    let parents = {};
    let minway = findlowestprice(prices,processed);
    let price = 0;
    let newprice = 0;
    let neighbors = [];
    let isFinded = false;
    let fuel = 0;

    parents = graph[start].reduce( function(key, [c,n])     {
        return {...key, ...{[n] : start}};
    }
    ,{} ); 

    while(minway)    {
        neighbors = graph[minway];
        price = prices[minway];
        neighbors.forEach((n) =>    {
            let [oldprice, oldminway] = n;
            if(typeof prices[oldminway] == 'undefined') {
                prices[oldminway] = Infinity;
            }
            newprice = price + oldprice;
            if(prices[oldminway] > newprice)    {
                prices[oldminway] = newprice;
                parents[oldminway] = minway;
            }
        })         
        if(minway == goal)    {
            isFinded = true;
            break;
        }
        processed.push(minway);
        minway = findlowestprice(prices,processed);        
    }
    return isFinded ? parents : null;
}


function findlowestprice(prices,processed)  {
        let lowestprice = Infinity;
        let lowestpriceway = null;
        Object.keys(prices).forEach((key) =>        {
            
            if(prices[key] < lowestprice && !processed.includes(key))             {
                lowestprice = prices[key];
                fuel = lowestprice;
                lowestpriceway = key;  
            }
        });
        return lowestpriceway;
}

module.exports = {
    calculateRoverPath,
};
